using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerController : MonoBehaviour
{
    public GameObject obj1;
    public int obj1Num;
    public List<GameObject> obj1List;
    // Start is called before the first frame update
    void Start()
    {
        //Instanciar objeto 1
        for (int i = 0; i < obj1Num; i++)
        {
            GameObject aux;
            aux = GameObject.Instantiate(obj1, this.transform);
            obj1List.Add(aux);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
