using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletMovement : MonoBehaviour
{
    private Rigidbody2D rb;
    public GameObject jogador;
    public bool viradoPraDir;
    public float bulletSpeed;
    // Start is called before the first frame update
    void Start()
    {
        jogador = GameObject.FindGameObjectWithTag("Jogador");
        rb = GetComponent<Rigidbody2D>();
        viradoPraDir = jogador.GetComponent<movement>().viradoPraDireita;
        if (viradoPraDir)
        {
            rb.AddForce(new Vector2(bulletSpeed, 0));
        }
        else if (!viradoPraDir)
        {
            rb.AddForce(new Vector2(-bulletSpeed, 0));
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "BreakableWall")
        {
            collision.gameObject.SetActive(false);
        }
    }
}