using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class movement : MonoBehaviour
{
    private Rigidbody2D rb;
    public float jumpForce;
    public float speed;
    public int moeda;
    public Text counterMoeda;
    public bool viradoPraDireita;
    public GameObject bullet;
    public bool onAir;
    private Animator anim;
    private Vector3 posInicial;
    public SpriteRenderer sprRend;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponentInChildren<Animator>();
        sprRend = GetComponentInChildren<SpriteRenderer>();
        posInicial = rb.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.X))
        {
            Instantiate(bullet, rb.transform.position, rb.transform.rotation);
        }

        if (Input.GetKeyDown(KeyCode.Z) && onAir == false)
        {
            rb.AddForce(new Vector2(0, jumpForce));
            onAir = true;
            anim.SetBool("Jumping", onAir);
        }

        float dirX = Input.GetAxis("Horizontal");
        rb.velocity = new Vector2 (dirX * speed, rb.velocity.y);
        anim.SetFloat("Walk", rb.velocity.x);

        if (rb.velocity.x == 0)
        {
            anim.SetBool("NotMoving", true);
        }
        else
        {
            anim.SetBool("NotMoving", false);
        }

        if (dirX > 0)
        {
            viradoPraDireita = true;
            sprRend.flipX = false;
        }
        if (dirX < 0)
        {
            viradoPraDireita = false;
            sprRend.flipX = true;
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Moeda")
        {
            collision.gameObject.SetActive(false);
            moeda++;
            counterMoeda.text = moeda.ToString();
        }
        if (collision.gameObject.tag == "Inferno")
        {
            rb.position = posInicial;
        }
        if (collision.gameObject.tag == "Checkpoint")
        {
            posInicial = rb.position;
        }
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "floor")
        {
            onAir = false;
            anim.SetBool("Jumping", onAir);
        }
    }
}
